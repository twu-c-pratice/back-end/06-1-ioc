package com.twuc.webApp.yourTurn;

import com.twuc.webApp.OutOfScanningScope;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class IoCCasesTest {
    private static AnnotationConfigApplicationContext context;

    @BeforeAll
    static void initApplicationContext() {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp.yourTurn");
    }

    @Test
    void should_generate_without_dependency() {
        WithoutDependency withoutDependency = context.getBean(WithoutDependency.class);
        assertSame(WithoutDependency.class, withoutDependency.getClass());
    }

    @Test
    void should_generate_with_dependency() {
        WithDependency withDependency = context.getBean(WithDependency.class);
        assertSame(WithDependency.class, withDependency.getClass());
        assertSame(Dependent.class, withDependency.getDependent().getClass());
    }

    @Test
    void should_get_exception_when_bean_out_of_range() {
        assertThrows(
                RuntimeException.class,
                () -> {
                    OutOfScanningScope outOfScanningScope =
                            context.getBean(OutOfScanningScope.class);
                });
    }

    @Test
    void should_get_bean_using_interface_implement() {
        Interface interfaceImpl = context.getBean(Interface.class);
        assertSame(InterfaceImpl.class, interfaceImpl.getClass());
    }

    @Test
    void should_get_bean_using_configuration() {
        SimpleObject simpleObject = (SimpleObject) context.getBean(SimpleInterface.class);
        assertEquals("O_o", simpleObject.getSimpleDependent().getName());
    }

    @Test
    void should_get_bean_with_auto_wired() {
        MultipleConstructor constructor = context.getBean(MultipleConstructor.class);
        assertEquals("Dependent constructor was called", constructor.getWhichConstructor());
    }

    @Test
    void should_called_autowired_method() {
        WithAutowiredMethod withAutowiredMethod = context.getBean(WithAutowiredMethod.class);
        List<String> list = new ArrayList<>();
        list.add("No argument constructor was called!");
        list.add("initialize was called!, argument is null: false");

        assertEquals(2, withAutowiredMethod.getLogs().size());
        assertIterableEquals(list, withAutowiredMethod.getLogs());
    }

    @Test
    void should_get_multiple_beans() {
        Map<String, InterfaceWithMultipleImpls> impls =
                context.getBeansOfType(InterfaceWithMultipleImpls.class);

        assertEquals(impls.get("implementationA").getClass(), ImplementationA.class);
        assertEquals(impls.get("implementationB").getClass(), ImplementationB.class);
        assertEquals(impls.get("implementationC").getClass(), ImplementationC.class);
    }
}
