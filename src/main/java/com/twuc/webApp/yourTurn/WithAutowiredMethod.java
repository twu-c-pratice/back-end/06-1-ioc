package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class WithAutowiredMethod {
    private Dependent dependent;
    private List<String> logs = new ArrayList<>();

    public WithAutowiredMethod() {
        logs.add("No argument constructor was called!");
    }

    public WithAutowiredMethod(Dependent dependent) {
        this.dependent = dependent;
        Boolean isNull = dependent == null;
        logs.add(
                String.format(
                        "Dependent argument constructor was called!, argument is null: %b",
                        isNull));
    }

    @Autowired
    public void initialize(AnotherDependent anotherDependent) {
        Boolean isNull = anotherDependent == null;
        logs.add(String.format("initialize was called!, argument is null: %b", isNull));
    }

    public List<String> getLogs() {
        return logs;
    }
}
