package com.twuc.webApp.yourTurn;

import org.springframework.stereotype.Component;

@Component
public class WithDependency {
    private Dependent dependent;

    public WithDependency(Dependent dependent) {
        this.dependent = dependent;
    }

    public Dependent getDependent() {
        return dependent;
    }
}
