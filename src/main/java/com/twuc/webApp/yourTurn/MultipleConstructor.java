package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MultipleConstructor {
    private String whichConstructor;

    @Autowired
    public MultipleConstructor(Dependent dependent) {
        whichConstructor = "Dependent constructor was called";
    }

    public MultipleConstructor(String string) {
        whichConstructor = "String constructor was called";
    }

    public String getWhichConstructor() {
        return whichConstructor;
    }
}
